
// Programa que muesta el incremento,decremento,post i pre incremento.
// Jordy Tirado Torres, 03/07/13, 11:19am.

public class Principal 
{

    public static void main(String[] args) 
    {
        
       int c;
       
       c = 5;
       
        System.out.println(c);
        System.out.println(c++);
        System.out.println(c);
        
        c = 5;
        
        System.out.println("\n"+c);
        System.out.println(++c);
        System.out.println(c);
        
    }// fin del main.
    
}// fin de la clase Principal.
