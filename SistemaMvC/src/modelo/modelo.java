package modelo;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author LuisVigo
 */
public class modelo extends database {

    private DefaultTableModel tablemodel = new DefaultTableModel();

    /** Constructor de clase */
    public modelo (){}

     /** Metodo privado para validar datos */
    private boolean valida_datos(String id, String nombre , String precio, String cantidad, String categoria)
    {
        if( id.equals("  -   ") )
            return false;
        else if( nombre.length() > 0 && precio.length()>0 && cantidad.length() >0  && categoria.length()>0)
        {
            return true;
        }
        else  return false;
    }

    /** Añade un nuevo registro a la base de datos */
    public boolean NuevoProducto(String id, String nombre , String precio, String cantidad, String categoria)
    {
        if( valida_datos( id, nombre, precio, cantidad, categoria ) )
        {
           
            precio = precio.replace(",", ".");
            //Se arma la consulta
            String q=" INSERT INTO tproducto ( p_id , p_descripcion , p_preciov, p_stock , id_categoria ) "
                + "VALUES ( '" + id + "','" + nombre + "', '" + precio + "'," + cantidad + " , '"+ categoria+"' ) ";
            //se ejecuta la consulta
            try {
                PreparedStatement pstm = this.getConexion().prepareStatement(q);
                pstm.execute();
                pstm.close();
                return true;
            }catch(SQLException e){
                System.err.println( e.getMessage() );
            }
            return false;
        }
        else return false;
    }

    /** Actualiza producto condicional WHERE el ID del producto */
    public boolean ActualizarProducto( String id, String nombre , String precio, String cantidad, String categoria )
    {
        //se reemplaza "," por "."
        precio = precio.replace(",", ".");
        String q= "UPDATE tproducto SET p_descripcion='"+nombre+"', p_preciov='"+precio+"' , p_stock='"+cantidad+"' , id_categoria='"+categoria+"' "
                + " WHERE p_id='"+id+"' ";
         //se ejecuta la consulta
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement(q);
            pstm.execute();
            pstm.close();
            return true;
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return false;
    }

    /**
     * Registra nueva venta ademas de detalle de ventas y actualiza stock de productos
     */
    public boolean RegistrarVenta( String idventa , Map productos , String total, String detalle , String nit )
    {
        try {
            //registra venta
            String q = " INSERT tventa (v_id, v_fecha, v_preciot, v_detalle, v_nit ) "
                + "values( '"+idventa+"', '"+getFecha()+"', '"+total.replace(",", ".")+"', '"+detalle+"' , '"+nit+"' );";
            PreparedStatement pstm = this.getConexion().prepareStatement(q);
            pstm.execute();
            pstm.close();
            //registra venta de productos
            Iterator it = productos.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry)it.next();
                carrito itm = (carrito) e.getValue();
                pstm = this.getConexion().prepareStatement(" INSERT INTO tpv (pv_producto,pv_venta,pv_cantidad,pv_precio)"
                        + " values( '"+itm.getIdproducto()+"' , '"+idventa+"' , '"+itm.getCantidad()+"' , '"+itm.getPrecio().replace(",", ".")+"' );");
                pstm.execute();
                pstm.close();                
                //actualiza stock
                pstm = this.getConexion().prepareStatement(" UPDATE tproducto SET p_stock=p_stock-"+itm.getCantidad()+" WHERE p_id='"+itm.getIdproducto()+"' ");
                pstm.execute();
                pstm.close();
            }
            return true;
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return false;
    }

    /**
     * Obtiene los datos de una venta dado su ID en un DefaultTableModel
     */
    public DefaultTableModel getTablaVenta( String idventa )
    {
      String q1= "SELECT count(*) as total "
              + " FROM tproducto INNER JOIN tpv ON p_id=pv_producto WHERE pv_venta='"+idventa+"'";

      String q2= "SELECT p_id,p_descripcion,pv_cantidad,pv_precio "
              + " FROM tproducto INNER JOIN tpv ON p_id=pv_producto WHERE pv_venta='"+idventa+"'";
     //realizamos la consulta sql y llenamos los datos en la matriz "Object[][] data"
      int registros = 0;
      String[] columNames = {"ID Producto","Descripcion","Cantidad","Precio Unitario","Precio Total"};
      //obtenemos la cantidad de registros existentes en la tabla y se almacena en la variable "registros"
      try{
         PreparedStatement pstm = this.getConexion().prepareStatement( q1 );
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.err.println( e.getMessage() );
      }
    //se crea una matriz con tantas filas y columnas que necesite
    Object[][] data = new String[registros][6];
      try{
         PreparedStatement pstm = this.getConexion().prepareStatement( q2 );
         ResultSet res = pstm.executeQuery();
         int i=0;
         while(res.next()){
                data[i][0] =res.getString( "p_id" );
                data[i][1] = res.getString( "p_descripcion" );
                data[i][2] =res.getString( "pv_cantidad" );
                data[i][3] =res.getString( "pv_precio" );
                data[i][4] = ""+(res.getFloat( "pv_precio" ) * res.getInt( "pv_cantidad" ));
            i++;
         }
         res.close();
         this.tablemodel.setDataVector(data, columNames );
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return this.tablemodel;
    }

    /**
     * Obtiene los datos de ventas segun un rango de fechas de la forma yyyy-MM-dd
     */
    public DefaultTableModel getDatosVentaxFechas( Date fecha1, Date fecha2  )
    {
      SimpleDateFormat _sdf= new SimpleDateFormat("yyyy-MM-dd");
      String q1= "SELECT count(*) as total FROM tventa INNER JOIN tcliente ON c_NIT=v_nit "
            + " WHERE date(v_fecha) BETWEEN '"+_sdf.format(fecha1)+"' and '"+_sdf.format(fecha2)+"' ";
      String q2= "SELECT v_id, v_fecha, c_NIT, c_nombre, v_preciot "
              + " FROM tventa INNER JOIN tcliente ON c_NIT=v_nit "
              + "WHERE date(v_fecha) BETWEEN '"+_sdf.format(fecha1)+"' and '"+_sdf.format(fecha2)+"' ";     
      int registros = 0;
      String[] columNames = {"ID Venta","Fecha","NIT","Cliente","Precio Total"};
      //obtenemos la cantidad de registros existentes en la tabla y se almacena en la variable "registros"
      try{
         PreparedStatement pstm = this.getConexion().prepareStatement( q1 );
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.err.println( e.getMessage() );
      }
    //se crea una matriz con tantas filas y columnas que necesite
    Object[][] data = new String[registros][6];
      try{
         PreparedStatement pstm = this.getConexion().prepareStatement( q2 );
         ResultSet res = pstm.executeQuery();
         int i=0;
         while(res.next()){
                data[i][0] = res.getString( "v_id" );
                data[i][1] = res.getString( "v_fecha" );
                data[i][2] = res.getString( "c_NIT" );
                data[i][3] = res.getString( "c_nombre" );
                data[i][4] = res.getString( "v_preciot" );
            i++;
         }
         res.close();
         this.tablemodel.setDataVector(data, columNames );
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return this.tablemodel;
    }

    /**
     * Obtiene los registros de los productos del inventario en un DefaultTableModel
     */
    public DefaultTableModel getTablaProducto()
    {      
      int registros = 0;
      String[] columNames = {"ID","Descripcion","Precio Venta","Stock","Categoria"};
      //obtenemos la cantidad de registros existentes en la tabla y se almacena en la variable "registros"
      try{
         PreparedStatement pstm = this.getConexion().prepareStatement( "SELECT count(*) as total FROM tproducto");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.err.println( e.getMessage() );
      }
    //se crea una matriz con tantas filas y columnas que necesite
    Object[][] data = new String[registros][6];
      try{
         PreparedStatement pstm = this.getConexion().prepareStatement("SELECT * FROM tproducto INNER JOIN tcategoria on c_id = id_categoria");
         ResultSet res = pstm.executeQuery();
         int i=0;
         while(res.next()){                         
                data[i][0] =res.getString( "p_id" );
                data[i][1] = res.getString( "p_descripcion" );
                data[i][2] =res.getString( "p_preciov" );
                data[i][3] =res.getString( "p_stock" );
                data[i][4] = res.getString( "c_id" ) + " - " + res.getString( "c_nombre" );
            i++;
         }
         res.close();
         this.tablemodel.setDataVector(data, columNames );
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return this.tablemodel;
    }

    /**
     * Obtiene las categorias del sistema en un DefaultComboBoxModel
     */
    public DefaultComboBoxModel getListaCategorias()
    {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
         try{
         PreparedStatement pstm = this.getConexion().prepareStatement("SELECT * FROM tcategoria ");
         ResultSet res = pstm.executeQuery();
         while(res.next()){
                model.addElement( res.getString( "c_id" ) + " - " + res.getString( "c_nombre" ) );
         }
         res.close();
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return model;
    }

    /**
     * Obtiene los registros de clientes NIT y nombre completo en un DefaultComboBoxModel
     */
    public DefaultComboBoxModel getListaClientes()
    {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
         try{
         PreparedStatement pstm = this.getConexion().prepareStatement("SELECT * FROM tcliente ");
         ResultSet res = pstm.executeQuery();
         while(res.next()){
                model.addElement( "NIT: - " + res.getString( "c_NIT" ) + " - " + res.getString( "c_nombre" ) );
         }
         res.close();
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return model;
    }


    /**
     * Genera un codigo aleatorio alfanumerico de 8 caracteres
     */
    public String getRandomCode()
    {
        String code = "";
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while ( i < 8){
            char c = (char)r.nextInt(255);
            if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') ){
                code += c;
                i ++;
            }
        }
        return code;
    }

    /**
     * obtiene la fecha actual del sistema en el formato de MYSQL
     */
    public String getFecha()
    {
        SimpleDateFormat _sdf= new SimpleDateFormat("yyyy-MM-dd");
        return _sdf.format(new Date());
    }

    public DefaultComboBoxModel getListaProductos()
    {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
         try{
         PreparedStatement pstm = this.getConexion().prepareStatement("SELECT p_descripcion FROM tproducto ");
         ResultSet res = pstm.executeQuery();         
         while(res.next()){                                                         
                model.addElement( res.getString( "p_descripcion" ) );
         }
         res.close();         
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }        
        return model;
    }

    /**
     * Dado un MAP que contiene clases "carrito.java" genera un DefaultComboBoxModel y retorna
     */
    public DefaultComboBoxModel ListaProductos( Map carrito_compra )
    {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        Iterator it = carrito_compra.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();
            carrito itm = (carrito) e.getValue();
            model.addElement( itm.getIdproducto() + " | " + itm.getDescripcion() + " | " + itm.getPrecio() + "           | " + itm.getCantidad() );
        }
        return model;
    }

    /**
     * dado un MAP que contiene clases "carrito.java" recorre todos los productos contenidos y calcula el total de la venta
     */
    public float getTotalVenta( Map carrito_compra )
    {
        float total=0;
        Iterator it = carrito_compra.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();
            carrito itm = (carrito) e.getValue();
            total += Float.valueOf(itm.getPrecio().toString()) * Integer.valueOf(itm.getCantidad().toString()) ;
        }
        return total;
    }
    
    /**
     * Obtiene los datos de un producto dado su nombre
     */
    public producto getProducto( String nombre )
    {
        producto p = new producto();
        String q = "SELECT * FROM tproducto WHERE p_descripcion = '"+nombre+"' ";
         try{
         PreparedStatement pstm = this.getConexion().prepareStatement(q);
         ResultSet res = pstm.executeQuery();
         while(res.next()){
            p.setId( res.getString( "p_id" ) );
            p.setDescripcion( res.getString( "p_descripcion" ) );
            p.setPrecio( res.getFloat( "p_preciov" ) );
            p.setStock( res.getInt( "p_stock" ) );
            p.setCategoria( res.getString( "id_categoria" ) );
         }
         res.close();
         }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return p;
    }

    /**
     * Obtiene en un string los datos generales de una venta dado su ID
     */
    public String getDatosVenta( String idventa )
    {
        String resultado="";         
        String q = "SELECT * FROM tventa INNER JOIN tcliente ON c_NIT=v_nit"
                + " WHERE v_id = '"+idventa+"' ";
        try{
         PreparedStatement pstm = this.getConexion().prepareStatement(q);
         ResultSet res = pstm.executeQuery();
         while(res.next()){
             resultado += "Cliente: " + res.getString("c_nombre") + "\n";
             resultado += "NºT : " + res.getString("c_nit") + "\n";
             resultado += "Dirección : " + res.getString("c_dir") + "\n";
             resultado += "Fecha : " + res.getString("v_fecha") + "\n";
             resultado +="----------------------------------------\n";
             resultado += "TOTAL [Soles.] : " + res.getString("v_preciot") + "\n";
             resultado += "Detalle: " + res.getString("v_detalle") + "\n";
        }
         res.close();
        }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return resultado;
    }

}
